'use strict';
// part__1
// 1. Напишите функцию, которая возвращает все числа от одного до ста, которые меньше, чем квадратный корень числа, которое ввёл пользователь.
// Выведите числа в консоль.
// Ввод числа проводите через prompt().

var userInput = prompt('напиши число!', 'number');

function numsLessSqrt(input) {
  var arr = [];
  if (input < 1) {
    console.log('ваше число ${input}. отрицательное число');
  } else {
    for (var i = 1; i < 100; i++) {
      if (i <= Math.sqrt(input)) {
        arr.push(i);
      }
    }
    return arr;
  }
}

if (userInput <= 0) {
  console.error('Ваше число не канает'); // console.error посоветовал использоать Женя
} 
  else {
  console.log('Возвращаемые значения', ...numsLessSqrt(userInput));
  console.log('');

  console.log('значения');
  for (var i = 0; i < numsLessSqrt(userInput).length; i++) {
    console.log(
      'Число',
      numsLessSqrt(userInput)[i],
      'меньше квадратного кореня указанного числа ',
      +userInput
    );
  }
}
