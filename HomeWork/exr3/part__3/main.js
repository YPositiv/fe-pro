'use strict';
// part__3
// 3. Пользователь вводит три числа и знак + или -. Верните в консоль большее / меньшее, в зависимости от введённого знака.Проверяйте все вводы пользователя.

var nums = [];
function addData(dat) {
  var a = 0;
  do {
    nums[a] = +prompt('Напиши цифру_' + (a + 1), 0);
    if (nums[a] !== nums[a]) {
      console.log('Ты забыл написать цифру!_');
      continue;
    }
    a++;
  } while (nums.length < dat);
  return nums;
}
addData(3);

var sign = prompt('напиши знак "+" или "-"', '+ или -');

switch (sign) {
  case '+':
    console.log(
      `Из указанных цифр (${nums}) большее ${Math.max(...nums)}`
    );
    break;
  case '-':
    console.log(
      `Из указанных цифр (${nums}) меньшее ${Math.min(...nums)}`
    );
    break;
  case null:
    console.log('ВААААЙ!!! Почему знак + или - не поставил?!');
    break;
  default:
    console.log('Ошибочка вышла');
    break;
}
