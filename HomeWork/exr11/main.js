// События.
// Замыкания и события.

// Изучить следующие пункты второй части учебника:
// Глава "Основы работы с событиями"(http://learn.javascript.ru/document):
//   2.1 - 2.8

// Просмотреть все примеры учебника, а так же решить задачи.

// Практические задания.
// 1. Создать простой интерфейс модального окна.Создайте кнопку, при нажатии на которую открывается модальное окно.Окно должно закрываться при нажатии на крестик закрытия, а так же при нажатии вне область окна.
// 2. Создайте группу аккордеонов(сворачивающиееся списки).У кажного списка есть заголовок при нажатии на который список раскрывается.В один момент времени может существовать только один открытый список.

let popup = document.querySelector('#drop--it'),
  popupToggle = document.querySelector('#popupBtn'),
  popupClose = document.querySelector('.close');

popupToggle.addEventListener('click', e => popup.style.display = 'block');
popupClose.addEventListener('click', e => popup.style.display = 'none');
window.addEventListener('click', e => {
  if (e.target == popup) {
    popup.style.display = 'none';
  }
});

// тут я маленько запутался, закрытие по нажатию на экран сделаю чуть позже.

let wrap = document.querySelector('.wrap');

wrap.addEventListener('click', toggleNav);

function toggleNav(event) {
  if (event.target.classList.contains('menu')) {
    let sub = this.querySelectorAll('.submenu');
    let visible = this.querySelector('.visible');
    let currentSub = event.target.parentNode.querySelector('.submenu');

    if (visible) {
      for (let i = 0; i < sub.length; i++) {
        sub[i].classList.remove('visible');
      }
    }
    currentSub.classList.add('visible');
  }
}
